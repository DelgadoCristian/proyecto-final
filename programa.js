function crearLista(){


    let elemento=document.createElement("li");
    let texto=document.createTextNode("sitio Javascript");

    elemento.appendChild(texto);

    let lista=document.getElementById(listaSitios)

    lista.appendChild(elemento);


 
}
function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.585520661102644,-74.14740979671478], zoom:18 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorBOGOTA= L.marker([4.585520661102644,-74.14740979671478]);
    marcadorBOGOTA.addTo(mapa);

  
    addTo(mapa);                            
}

function Mapear() {
    let mapa = L.map('divMapa', { center: [4.586921638479058,-74.14770483970642], zoom:18 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorBOGOTA= L.marker([4.586921638479058,-74.14770483970642]);
    marcadorBOGOTA.addTo(mapa);

  
    addTo(mapa);
}

function Localizar() {
    let mapa = L.map('divMapa', { center: [4.5869483746618105,-74.14765119552612], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    


    var polygon = L.polygon([
        [4.589793098786352,-74.14553761482239],
        [4.58791087657419, -74.15001153945921],
        [4.5876649039866475, -74.15002226829529],
        [4.586007260426522, -74.1511595249176],
        [4.5849378109267915, -74.15092349052429],
        [4.585247951446538, -74.14972186088562],
        [4.584595586748775,-74.1495931148529],
        [4.584488641659521,-74.14966821670532],
        [4.583526135136049,-74.14939999580383],
        [4.586541984575974,-74.14455056190491],
        [4.589793098786352, -74.14553761482239]
    ]).addTo(mapa);
}

function ubicar() {
    let mapa = L.map('divMapa', { center: [4.589140738240688,-74.14695918560028], zoom:18 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    
    let marcadorBOGOTA= L.marker([4.589140738240688,-74.14695918560028]);
    marcadorBOGOTA.addTo(mapa);

  
    addTo(mapa);
}

function Visitar() {
    let mapa = L.map('divMapa', { center: [4.586921638479058,-74.14770483970642], zoom:15 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    var Polideportivo = L.marker([4.585520661102644,-74.14740979671478], {
        title: "Polideportivo",draggable:true,
        opacity: 1
        }).bindPopup("<h4>Polideportivo</h4><li><a href=Evento1.html>Deporte</li></a>")
        .addTo(mapa);
       
    var Capilla = L.marker([4.586921638479058,-74.14770483970642], {
            title: "Capilla",draggable:true,
            opacity: 1
            }).bindPopup("<h4>Capilla</h4><li><a href=Evento2.html>Donaciones</li></a>")
            .addTo(mapa);
   
     var BarradeJavi = L.marker([4.589140738240688,-74.14695918560028], {
            title: "BarradeJavi",draggable:true,
            opacity: 1
            }).bindPopup("<h4>LaBarradeJavi</h4><li><a href=Evento3.html>Re apertura</li></a>")
            .addTo(mapa);

    
    var polygon = L.polygon([
        [4.589793098786352,-74.14553761482239],
        [4.58791087657419, -74.15001153945921],
        [4.5876649039866475, -74.15002226829529],
        [4.586007260426522, -74.1511595249176],
        [4.5849378109267915, -74.15092349052429],
        [4.585247951446538, -74.14972186088562],
        [4.584595586748775,-74.1495931148529],
        [4.584488641659521,-74.14966821670532],
        [4.583526135136049,-74.14939999580383],
        [4.586541984575974,-74.14455056190491],
        [4.589793098786352, -74.14553761482239]
    ]).addTo(mapa);
  
    
}

function guardarDatos(){
    localStorage.Organizador= document.getElementById("Organizador").value;
    localStorage.Evento= document.getElementById("Evento").value;
    localStorage.Descripcion= document.getElementById("Descripcion").value;
    localStorage.Fecha= document.getElementById("Fecha").value;
   
}